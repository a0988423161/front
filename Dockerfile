FROM node:12-alpine
WORKDIR /usr/src/app

COPY package.* .

RUN npm install -g --unsafe-perm preact-cli

RUN npm install

CMD ["npm", "run", "dev"]

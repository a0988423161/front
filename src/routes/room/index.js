import { h } from 'preact';
import style from './style';
import { useState, useEffect, useLayoutEffect } from 'preact/hooks';

import axios from '../../request';
import io from 'socket.io-client';

import VideoPlayer from '../../components/videoPlayer'
import ListUsers from '../../components/listUsers';
import ListVideos from '../../components/listVideos';

import Modal from '../../components/Modal';

import HeaderRoom from '../../components/headerRoom';

import ToggleInputContent from '../../components/ToggleInputContent';

import ChatBox from '../../components/ChatBox';

import Notification from '../../components/Notification';

const roomLoginEvent = "room:login";
//const roomDisconnectEvent = "disconnect";

const videoAddEvent = "video:add";
//const videoEditEvent = "video:edit";
const videoDeleteEvent = "video:delete";

const videoAutoPlayEvent = "video:autoplay";
const videoPlayStatusEvent = "video:playStatus";
const videoPlayerTimeEvent = "video:playerTime";

const videoMoveUp = "video:moveUp";
const videoMoveDown = "video:moveDown";

const roomPasswordEvent = "room:setPassword";
const roomAdminPasswordEvent = "room:setAdminPassword";

const roomIsPasswordProtectedEvent = "room:setIsPasswordProtected";
const roomIsAdminPasswordProtectedEvent = "room:setIsPasswordProtected";

const videoOnSeekEvent = "video:seek";

const roomLoadEvent = "room:load";
const messageEvent = "message";

const roomToggleUserPermEvent = "room:toggleUserPerm";

const usersConnectedEvent = "users:count";
const usersDisconnectedEvent = "users:disconnect";
const usersGetInfosEvent = "users:getInfos";

const roomAskAuthEvent = "room:askauth";

const success = ":success";
const err = ":err";

const baseSocketAPI = "https://api-socket.dev.gvw.grosseteub.fr";
//const baseSocketAPI = "http://172.27.0.6:1337";

const Room = ({id}) => {
    const [token, setToken] = useState();
    const [usersConnected, setUsersConnected] = useState([]);
    const [socket, setSocket] = useState(io(`${baseSocketAPI}/room-${id}`));
    const [room, setRoom] = useState({});

    const [roomAdministrators, setRoomAdministrators] = useState([]);
    const [playlist, setPlaylist] = useState([]);
    
    const [isAdmin, setIsAdmin] = useState(false);

    const [myID, setMyID] = useState();

    const [autoPlay, setAutoPlay] = useState(false);
    const [playing, setPlaying] = useState(autoPlay);

    const [timeOnLoad, setTimeOnLoad] = useState(0);

    const [seek, setSeek] = useState(0);

    const [password, setPassword] = useState();
    
    const [roomPassword, setRoomPassword] = useState();
    const [roomAdminPassword, setRoomAdminPassword] = useState();

    const [isRoomPasswordProtected, setIsRoomPasswordProtected] = useState(false);
    const [isRoomAdminPasswordProtected, setIsRoomAdminPasswordProtected] = useState(false);
    
    const [ModalPassword, setModalPassword] = useState(false);

    const [refreshing, setRefreshing] = useState(false);

    const [errors, setErrors] = useState([]);

    const [TypeInput, setTypeInput] = useState("password");

    const [messages, setMessages] = useState([]);
    
    const RoomLogin = () => {
	axios.post('/room/login', {
            id,
            password
	}).then((response) => {
            setRefreshing(false);
            setModalPassword(false);
            socket.emit("room:login", response.data.token);
            setToken(response.data.token);
	}).catch((error) => {
	    setRefreshing(false);
            console.log(error);
            console.log(error.response.data);
	    if (error.response.data.errors[0].msg === "PASSWORD_IS_NEEDED")
                setModalPassword(true);
            else
                setErrors(error.response.data.errors);
	});
    }
    
    useEffect(() => {
	//setSocket(io(`${baseSocketAPI}/room-${id}`));
	//RoomLogin();
	return function cleanup() {
	    console.log("CleanUp");
	    socket.close();
	}
    }, []);

    useLayoutEffect(() => {
	RoomLogin();
    }, [refreshing]);

    
    socket.off('connect').on('connect', () => {
	console.log("Connected");
    });

    socket.off('disconnect').on('disconnect', () => {
	console.log("Disconnected");
    });

    socket.off(roomLoginEvent + success).on(roomLoginEvent + success, (data) => {
	console.log(data);
	setMyID(data.userID);
	socket.emit(roomLoadEvent);
    });

    socket.off(roomLoginEvent + err).on(roomLoginEvent + err, (data) => {
	console.log(`err login ${  data}`);
    });

    socket.off(roomLoadEvent + success).on(roomLoadEvent + success, (data) => {
	console.log(`load ${  JSON.stringify(data)}`);
	setRoom(data);
	setRoomAdministrators(data.administrators);
	setIsRoomPasswordProtected(data.isPasswordProtected);
	//setIsRoomAdminPasswordProtected(data.isAdminPasswordProtected);
	setPlaylist(data.playlist);
	setAutoPlay(data.videoPlayer.autoPlay);
	if (data.videoPlayer.playerState === "true")
	    setPlaying(true);
	else if (data.administrators.indexOf(myID) > -1){
	    setPlaying(Boolean(data.videoPlayer.autoPlay));
	    setPlayerState(Boolean(data.videoPlayer.autoPlay));
	}
	console.log("myID: " + myID);
	console.log("admin" + roomAdministrators);
	if (data.administrators.indexOf(myID) > -1)
	    setIsAdmin(true);
	setTimeOnLoad(data.videoPlayer.playerTime);
	socket.emit(usersGetInfosEvent, data.usersConnected);
    });

    socket.off(roomLoadEvent + err).on(roomLoadEvent + err, (data) => {
        console.log(`err load ${  data}`);
    });

    socket.off(videoAddEvent + success).on(videoAddEvent + success, (video) => {
        console.log(`Your video was added ${  JSON.stringify(video)}`);
	if (playlist.includes(video) === false)
	    setPlaylist(playlist => [...playlist, video]);
    });

    socket.off(videoAddEvent + err).on(videoAddEvent + err, (test) => {
        console.log(`err add ${  test}`);
    });
    
    socket.off(videoDeleteEvent + success).on(videoDeleteEvent + success, (data) => {
	console.log(JSON.stringify(data));
        console.log(`Your video was deleted ${  data.id}`);
	console.log(playlist.filter(video => video._id !== data.id));
	setPlaylist(playlist.filter(video => video._id !== data.id));
    });

    socket.off(videoDeleteEvent + err).on(videoDeleteEvent + err, (test) => {
        console.log(`err delete ${  test}`);
    });

    socket.off(videoAutoPlayEvent + success).on(videoAutoPlayEvent + success, (data) => {
	setAutoPlay(data);
    });
    
    socket.off(videoAutoPlayEvent + err).on(videoAutoPlayEvent + err, (data) => {
	    console.log(data);
    });

    socket.off(usersGetInfosEvent + success).on(usersGetInfosEvent + success, (data) => {
	for (let i = 0; i < data.length; i+=1){
	    let found = false;
	    if (usersConnected.length > 0){
		for (let n = 0; n < usersConnected.length; n+=1)
		{
		    console.log('ok');
		    console.log(usersConnected[n]);
		    if (usersConnected[n].id === data[i].id)
		    {
			found = true;
			console.log(found);
		    }
		}
	    }
	    if (found === false)
		setUsersConnected([...usersConnected, data[i]]);
	}
    });

    socket.off(usersGetInfosEvent + err).on(usersGetInfosEvent + err, (data) => {
	console.log(data);
    });

    socket.off(videoOnSeekEvent + success).on(videoOnSeekEvent + success, (data) => {
	console.log(data);
    });
    
    socket.off(messageEvent + success).on(messageEvent + success, (data) => {
	console.log(data);
	setMessages([...messages, data]);
    });

    socket.off(messageEvent + err).on(messageEvent + err, (data) => {
	console.log(`err${  data}`);
    });

    socket.off(roomIsPasswordProtectedEvent + success).on(roomIsPasswordProtectedEvent + success, (data) => {
        console.log(data);
	setIsRoomPasswordProtected(data);	
    });

    socket.off(roomIsPasswordProtectedEvent + err).on(roomIsPasswordProtectedEvent + err, (data) => {
	console.log(data);
    });

    socket.off(roomPasswordEvent + success).on(roomPasswordEvent + success, (data) => {
	console.log(data);
	setRoomPassword('');
    });

    socket.off(roomPasswordEvent + err).on(roomPasswordEvent + err, (data) => {
	console.log(data);
    });

    socket.off(roomIsAdminPasswordProtectedEvent + success).on(roomIsAdminPasswordProtectedEvent + success, (data) => {
        console.log(data);
	setIsRoomAdminPasswordProtected(data);	
    });

    socket.off(roomIsAdminPasswordProtectedEvent + err).on(roomIsAdminPasswordProtectedEvent + err, (data) => {
	console.log(data);
    });

    socket.off(roomAdminPasswordEvent + success).on(roomAdminPasswordEvent + success, (data) => {
	console.log(data);
	setRoomAdminPassword('');
    });

    socket.off(roomAdminPasswordEvent + err).on(roomAdminPasswordEvent + err, (data) => {
	console.log(data);
    });

    socket.off(roomToggleUserPermEvent + success).on(roomToggleUserPermEvent + success, (data) => {
	setRoomAdministrators(data);
	if (data.indexOf(myID) > -1)
            setIsAdmin(true);
	else
	    setIsAdmin(false);
    });

    socket.off(videoMoveUp + success).on(videoMoveUp + success, (id) => {
	console.log("moveUp:success");
	moveUp(id);
    });

    socket.off(videoMoveDown + success).on(videoMoveDown + success, (id) => {
	console.log("moveDown:success");
	moveDown(id);
    });

    
    /*blablabla*/
    
    socket.off(usersConnectedEvent).on(usersConnectedEvent, (data) => {
	console.log(data);
	socket.emit(usersGetInfosEvent, data);
    });

    socket.off(usersDisconnectedEvent).on(usersDisconnectedEvent, (data) => {
	console.log("Someone is disconnected");
	setUsersConnected(usersConnected.filter(user => user.id !== data));
    });

    socket.off(videoAddEvent).on(videoAddEvent, (video) => {
	console.log(`Someone add video ${  JSON.stringify(video)}`);
	if (playlist.includes(video) === false)
	    setPlaylist(playlist => [...playlist, video]);
    });
    
    socket.off(videoDeleteEvent).on(videoDeleteEvent, (data) => {
	console.log(`Someone delete video ${  data}`);
	setPlaylist(playlist.filter(video => video._id !== data.id));
    });
    
    socket.off(videoAutoPlayEvent).on(videoAutoPlayEvent, (data) => {
	setAutoPlay(data);
    });

    socket.off(videoPlayStatusEvent).on(videoPlayStatusEvent, (data) => {
	setPlaying(data);
    });

    socket.off(videoOnSeekEvent).on(videoOnSeekEvent, (data) => {
	console.log(data);
	setSeek(data);
    });
    
    socket.off(messageEvent).on(messageEvent, (data) => {
	console.log(data);
	setMessages(messages => [...messages, data]);
    });
    
    socket.off(roomIsPasswordProtectedEvent).on(roomIsPasswordProtectedEvent, (data) => {
        console.log(data);
	setIsRoomPasswordProtected(data);
    });

    socket.off(roomPasswordEvent).on(roomIsPasswordProtectedEvent, (data) => {
	console.log(`Password Updated ${  data}`);
    });
    
    socket.off(roomAskAuthEvent).on(roomAskAuthEvent, () => {
	socket.emit("room:login", token);
    });

    socket.off(roomToggleUserPermEvent).on(roomToggleUserPermEvent, (data) => {
	setRoomAdministrators(data);
	if (data.indexOf(myID) > -1)
            setIsAdmin(true);
	else
	    setIsAdmin(false);
    });

    socket.off(videoMoveUp).on(videoMoveUp, (id) => {
	moveUp(id);
    });

    socket.off(videoMoveDown).on(videoMoveDown, (id) => {
	moveDown(id);
    });
    
    const moveUp = (id) => {
        console.log(id);
        let video = playlist.filter(video => video._id === id)[0];
        let videoIndex = playlist.indexOf(video);
        let playlistTmp = playlist;
        if (videoIndex > 0)
        {
            playlistTmp[videoIndex] = playlistTmp[videoIndex - 1];
            playlistTmp[videoIndex - 1] = video;
	    
            setPlaylist(playlistTmp.concat([]));
            console.log(playlist);
        }
    }

    const moveDown = (id) => {
        console.log(id);
        let video = playlist.filter(video => video._id === id)[0];
        let videoIndex = playlist.indexOf(video);
        let playlistTmp = playlist;
        if (videoIndex < playlist.length - 1)
        {
            playlistTmp[videoIndex] = playlistTmp[videoIndex + 1];
            playlistTmp[videoIndex + 1] = video;
            setPlaylist(playlistTmp.concat([]));
            console.log(playlist);
        }
    }

    
    const addVideo = (url) => {
	if (isAdmin === true)
	    socket.emit("video:add", url);
    }

    const deleteVideo = (id) => {
	if (isAdmin === true)
	    socket.emit("video:delete", id);
    }

    const setPlayerState = (state) => {
	if (isAdmin === true)
	    socket.emit("video:playStatus", state);
    }

    const setSeekTime = (time) => {
	if (isAdmin === true)
	    socket.emit("video:seek", time);
    }

    const setProgressTime = (time) => {
	if (isAdmin === true)
	    socket.emit(videoPlayerTimeEvent, time);
    }
    
    const toggleAutoPlay = () => {
	if (isAdmin === true)
	    socket.emit("video:autoplay", !autoPlay);
    }

    const handleKeyDown = (e) => {
	if (e.key === 'Enter') {
            setRefreshing(true);
        }
    }

    const sendMessage = (message, time) => {
	socket.emit(messageEvent, message, time);
    }

    const setSocketIsPasswordProtected = (state) => {
	if (isAdmin === true)
	    socket.emit(roomIsPasswordProtectedEvent, state);
    }
    
    const setSocketRoomPassword = () => {
	if (isAdmin === true)
	    socket.emit(roomPasswordEvent, roomPassword);
    }

    const setSocketRoomAdminPassword = () => {
	console.log(roomAdminPassword);
	if (isAdmin === true)
	    socket.emit(roomAdminPasswordEvent, roomAdminPassword);
    }

    const toggleUserPerm = (userID) => {
	if (isAdmin === true)
	    socket.emit(roomToggleUserPermEvent, userID);
    }

    const setSocketMoveUp = (id) => {
	console.log("moveUp");
	if (isAdmin === true)
	    socket.emit(videoMoveUp, id);
    }

    const setSocketMoveDown = (id) => {
	console.log("moveDown");
	if (isAdmin === true)
	    socket.emit(videoMoveDown, id);
    }
    
    return (
	    <div class={style.all}>
	    <Modal
        show={ModalPassword}
        handleClose={() => setModalPassword(false)}>
	    <h4>{room.title}</h4>
            <p>Enter Room Password</p>
	    {
                errors.map((error) => (
                        <p>{error.msg}</p>
                ))
            }
            <div>
	    <ToggleInputContent TypeInput={TypeInput} setTypeInput={setTypeInput}>
            <input type={TypeInput} name="password"  onInput={(e) => setPassword(e.target.value)} onKeyDown={handleKeyDown} onFocus={(e) => e.target.select()} autofocus />
	    </ToggleInputContent>
            <button type="submit" onClick={() => setRefreshing(true)}>Submit</button>
          </div>
            </Modal>
	    <div class={style.headers}>
	    {isAdmin &&
	     <HeaderRoom title={room.title} autoPlay={autoPlay} setAutoPlay={toggleAutoPlay} id={id} password={roomPassword} setPassword={setRoomPassword} setSocketPassword={setSocketRoomPassword} passwordAdmin={roomAdminPassword} setPasswordAdmin={setRoomAdminPassword} setSocketAdminPassword={setSocketRoomAdminPassword} isPasswordProtected={isRoomPasswordProtected} setIsPasswordProtected={setSocketIsPasswordProtected} />
	    }
	    <h1>{room.title}</h1>
	    <p>{room.description}</p>
	    </div>
	    {
		errors.map((error) => (
                        <Notification color="red" show="true">
			<p>{error.msg}</p>
			</Notification>
                ))
	    }
	    {usersConnected.length <= 1 &&
	     <Notification color="yellow" show="true">
	     <h3>You are alone in the room</h3>
	     <p>If you leave the room, it will be delete after 5 minutes.</p>
	     </Notification>
	    }
	    {Boolean(room.title) &&
	     <div class={style.room}>
	     <ListUsers class={style.listUsers}  usersConnected={usersConnected} admin={roomAdministrators} toggleUserPerm={toggleUserPerm} />
	     <VideoPlayer class={style.VideoPlayer} playlist={playlist} setPlayerState={setPlayerState} playing={playing} setPlaying={setPlaying} setSeekTime={setSeekTime} setProgressTime={setProgressTime} isAdmin={isAdmin} deleteVideo={deleteVideo} autoPlay={autoPlay} timeOnLoad={timeOnLoad} seek={seek} setSeek={setSeek} />
	     <ListVideos class={style.Playlist} playlist={playlist} setPlaylist={setPlaylist} addVideo={addVideo} deleteVideo={deleteVideo} isAdmin={isAdmin} moveUp={setSocketMoveUp} moveDown={setSocketMoveDown} />
	     <div class={style.break} />
	     <ChatBox class={style.ChatBox} messages={messages} sendMessage={sendMessage} usersConnected={usersConnected} myID={myID} />
	     </div>
	    }
	</div>
    );
}

export default Room;

import { h } from 'preact';
import style from './style';
import { useState } from 'preact/hooks';

import axios from '../../request';

import { route } from 'preact-router';

import ToggleInputContent from '../../components/ToggleInputContent';

const CreateRoom = () => {
    const [title, setTitle] = useState();
    const [description, setDescription] = useState();

    const [password, setPassword] = useState();
    const [passwordAdmin, setPasswordAdmin] = useState();

    const [isPassword, setIsPassword] = useState(false);
    const [isPasswordAdmin, setIsPasswordAdmin] = useState(false);
    
    const [errors, setErrors] = useState([]);

    const [TypeInput, setTypeInput] = useState("password");
    const [TypeInputAdmin, setTypeInputAdmin] = useState("password");

    
    const handleTitle = (event) => {
	setTitle(event.target.value);
    }
    
    const handleDescription = (event) => {
	setDescription(event.target.value);
    }

    const onClick = () => {
	//console.log(username + password);
	
	axios.post('/room/create', {
	    title,
	    description
	})
	    .then((response) => {
		//console.log(response.data.token);
		if (password === undefined && passwordAdmin === undefined)
		    route(`/room/${response.data.roomID}`);
		else {
		    axios.put('/room/edit', {
			id: response.data.roomID,
			isPasswordProtected: isPassword,
			password,
			newPasswordAdmin: passwordAdmin
		    }).then((res) => {
			//route(`/room/${  response.data.roomID}`);
			console.log(res.data);
			route(`/room/${response.data.roomID}`);
			}).catch((error) => {
			    console.log(error);
			    setErrors(error.response.data.errors);
			});
		}
	    })
	    .catch((error) => {
		console.log(error);
		setErrors(error.response.data.errors);
	    });
    }
    
    return (
	<div class={style.login}>
	    <h1>Create Room</h1>
	    {
		errors.map((error) => (
                        <p>{error.msg}</p>
                ))
            }
	    <div class={style.row}>
	      <div class={style.col-25}>
	         <label>Title:</label>
	      </div>
	      <div class={style.col-75}>
	         <input type="text" name="title" onInput={handleTitle} />
	      </div>
	    </div>
            <div class={style.row}>
	      <div class={style.col-25}>
	        <label>Description:</label>
	      </div>
	      <div class={style.col-75}>
	        <textarea type="text" name="description"  onChange={handleDescription} />
              </div>
	    </div>
	    <div class={style.row}>
	    <label>Password Protected</label><br />
	    <label class={style.switch}>
	    <input type="checkbox" checked={isPassword} onClick={() => { setIsPassword(!isPassword);}} />
	    <span class={`${style.slider  } ${  style.round}`} />
	    </label>
	    </div>
	    {isPassword &&
	     <div class={style.row}>
	     <div class={style.col-25}>
             <label>Room Password:</label>
             </div>
             <div class={style.col-75}>
	     <ToggleInputContent TypeInput={TypeInput} setTypeInput={setTypeInput}>
             <input type={TypeInput} name="password" onInput={(e) => setPassword(e.target.value)} />
	     </ToggleInputContent>
             </div>
             </div>
	    }
	    <div class={style.row}>
	    <label>Admin Password Protected</label><br />
	    <label class={style.switch}>
	    <input type="checkbox" checked={isPasswordAdmin} onClick={() => { setIsPasswordAdmin(!isPasswordAdmin);}} />
	    <span class={`${style.slider  } ${  style.round}`} />
	    </label>
	    </div>
	    {isPasswordAdmin &&
	     <div class={style.row}>
	     <div class={style.col-25}>
             <label>Room Admin Password:</label>
             </div>
             <div class={style.col-75}>
	     <ToggleInputContent TypeInput={TypeInputAdmin} setTypeInput={setTypeInputAdmin}>
             <input type={TypeInputAdmin} name="passwordAdmin" onInput={(e) => setPasswordAdmin(e.target.value)} />
	     </ToggleInputContent>
             </div>
             </div>
	    }
	    <div class={style.row}>
	       <button type="submit" onClick={onClick}>Submit</button>
	    </div>
	</div>
    );
}

export default CreateRoom;

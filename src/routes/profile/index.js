import { h } from 'preact';
import { useState, useEffect } from 'preact/hooks';

import style from './style';

import axios from '../../request';

import User from 'preact-icons/fa/user';
import Edit from 'preact-icons/fa/edit';

import Modal from '../../components/Modal';

import ToggleInputContent from '../../components/ToggleInputContent';

//export default class Profile extends Component {
const Profile = () => {
    const [username, setUsername] = useState();
    const [created, setCreated] = useState(new Date());
    const [updated, setUpdated] = useState(new Date());

    const [isModalOpen, setIsModalOpen] = useState(false);
    const [newUsername, setNewUsername] = useState();
    const [newPassword, setNewPassword] = useState();
    const [confirmNewPassword, setConfirmNewPassword] = useState();

    const [errors, setErrors] = useState([]);
    const [refresh, setRefresh] = useState(false);

    const [TypeInput, setTypeInput] = useState("password");
    const [TypeInputConfirm, setTypeInputConfirm] = useState("password");
    
    const handleSubmit = () => {
	if (newPassword === confirmNewPassword)
	{
	    setErrors([]);
	    let data = {
		username: newUsername,
		password: newPassword
	    }
	    axios.put('/user/edit', data)
		.then((response) => {
                    console.log(response.data);
		    setNewUsername('');
		    setNewPassword('');
		    setConfirmNewPassword('');
		    setRefresh(true);
		    setIsModalOpen(false);
		})
		.catch((error) => {
                    console.log(error);
		    console.log(error.response.data);
                    setErrors(error.response.data.errors);
		});
	}
	else
	    setErrors([{msg: `Password confirmation doesn't match`}]);
    }

    const hideModal = () => {
	setIsModalOpen(false);
    }

    const handleKeyDown = (event) => {
        if (event.key === 'Enter') {
            handleSubmit();
        }
    }

    
    useEffect(() => {
	axios.get('/user/get')
	    .then((response) => {
		// handle success
		setRefresh(false);
		console.log(response);
		setUsername(response.data.username);
		setCreated(new Date(response.data.created));
		setUpdated(new Date(response.data.updated));
	    })
	    .catch((error) => {
		// handle error
		console.log(error);
	    })
    }, [refresh]);

    return (
	    <div class={style.profile}>
	    <Modal
	show={isModalOpen}
	handleClose={hideModal}>
            <h4>Edit Profile</h4>
	    {
		errors.map((error) => (
			<p>{error.msg}</p>
		))
            }
            <div>
	    <label>New username</label>
	    <input type="text" value={newUsername} onInput={(e) => setNewUsername(e.target.value)} onKeyDown={handleKeyDown} />
	    </div>
	    <div>
	    <label>New password</label>
	    <ToggleInputContent TypeInput={TypeInput} setTypeInput={setTypeInput}>
	    <input type={TypeInput} value={newPassword} onInput={(e) => setNewPassword(e.target.value)} onKeyDown={handleKeyDown} />
	    </ToggleInputContent>
	    </div>
	    <div>
	    <label>Confirm new password</label>
	    <ToggleInputContent TypeInput={TypeInputConfirm} setTypeInput={setTypeInputConfirm}>
	    <input type={TypeInputConfirm} value={confirmNewPassword} onInput={(e) => setConfirmNewPassword(e.target.value)} onKeyDown={handleKeyDown} />
	    </ToggleInputContent>
	    </div>
            <button type="submit" onClick={handleSubmit}>Submit</button>
            </Modal>
	    <User size={32} /><Edit onClick={() => setIsModalOpen(true)} />
	    <h1>{username}</h1>
	    
	    <div>Created time: {created.toLocaleString()}</div>
	    <div>Updated time: {updated.toLocaleString()}</div>
	    
	</div>
    );
}

export default Profile;

import ClearX from "clearx";

let store = new ClearX({
  token: '',
  username: 'John Doe'
});

export default store;

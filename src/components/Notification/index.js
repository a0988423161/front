import { h } from 'preact';
import style from './style';

import Close from 'preact-icons/fa/close';

const Notification = ({show, color, children}) => {
    const showHideClass = show ? style.displayBlock : style.displayNone;

    const displayColor = {"red": "#f44336", "blue": "#2196F3", "green": "#4CAF50", "yellow": "#ff9800"};

    return (
	    <div class={`${style.alert  } ${  showHideClass}`} style={{backgroundColor: displayColor[color]}} >
            {children}
	    </div>
    );
}
//<Close  onClick={handleClose} />
export default Notification;

import { h } from 'preact';
import style from './style';

import UserRoomItem from '../../components/userRoomItem';

const ListUsers = ({usersConnected, admin, toggleUserPerm}) => {
    return (
	    <div class={style.listUser}>
            <h4 class={style.title}><b>List Users</b> ( {usersConnected.length}  )</h4>
	    <div class={style.wrapper}>
        {
            usersConnected.map((user) => {
		if(admin.indexOf(user.id) > -1){
		    return (
			    <UserRoomItem userID={user.id} isAdmin={true} username={user.username} toggleUserPerm={toggleUserPerm} />
		    );
		}
		
		return (
			<UserRoomItem userID={user.id} isAdmin={false} username={user.username} toggleUserPerm={toggleUserPerm} />
		);
		
	    })
	}
	</div>
	</div>
    );
}

export default ListUsers;

import { h } from 'preact';
import style from './style';

import Close from 'preact-icons/fa/close';
import Up from 'preact-icons/fa/caret-up';
import Down from 'preact-icons/fa/caret-down';

const VideoRoomItem = ({title, id, deleteVideo, isAdmin, moveUp, moveDown, arrayOrder, arraySize}) => {
    const onClick = () => {
	deleteVideo(id);
    }

    const up = () => {
	moveUp(id);
    }

    const down = () => {
	moveDown(id);
    }
    
    return (
	    <div class={style.card}>
	    <div class={style.container}>
	    {isAdmin &&
	        <Close onClick={onClick} />
	    }
	    <h4><b>{title}</b></h4>
	    </div>
	    {isAdmin &&
	     <>
	     {arrayOrder !== 0 &&
	      <Up onClick={up} />
	     }
	     {arrayOrder < arraySize - 1 &&
		     <Down onClick={down} />
	     }
	     </>
	    }
	</div>
    );
}

export default VideoRoomItem;

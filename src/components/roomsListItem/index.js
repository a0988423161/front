import { h } from 'preact';
import style from './style';

import { route } from 'preact-router';

import Lock from 'preact-icons/fa/lock';

const roomsListItem = ({id, title, description, isPasswordProtected}) => {
    const onCardPress = () => {
	    route(`/room/${  id}`);
    }
    
    return (
        <div>
	    <div class={style.card} onClick={onCardPress}>
	    <div class={style.container}>
	    { isPasswordProtected &&
	      <Lock class={style.lock} size={32} />}
	    <h4><b>{title}</b></h4>
	    <p>{description}</p>
	    </div>
	    </div>
	</div>
    );
}

export default roomsListItem;

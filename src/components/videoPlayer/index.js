import ReactPlayer from 'react-player';
import { h } from 'preact';
import style from './style';
import { useState, useRef } from 'preact/hooks';

import store from "../../store";

import Play from 'preact-icons/fa/play';
import Pause from 'preact-icons/fa/pause';
import Next from 'preact-icons/fa/arrow-right';

const VideoPlayer = ({isAdmin, playlist, setSeekTime, setProgressTime, deleteVideo, playing, setPlaying, setPlayerState, autoPlay, timeOnLoad, seek, setSeek}) => {
    //const [seeking, setSeeking] = useState();
    const [played, setPlayed] = useState(0);

    const [controls] = useState(isAdmin);
    
    const player = useRef(null);

    const [url, setUrl] = useState();
    const [onLoad, setOnLoad] = useState(true);

    if (playlist[0] !== undefined)
	setUrl(playlist[0].url);
    if (seek > 0){
	player.current.seekTo(parseFloat(seek));
	setPlayed(parseFloat(seek));
	setSeek(0);
    }
    
    /*const handleSeekMouseDown = e => {
	setSeeking(true);
	//player.current.seekTo(parseFloat(e.target.value));
    }

    const handleSeekChange = e => {
	setPlayed(parseFloat(e.target.value));
	setSeekTime(parseFloat(e.target.value));
    }

    const handleSeekMouseUp = e => {
	setSeeking(false);
	player.current.seekTo(parseFloat(e.target.value));
    }*/

     
    /*<input
      type='range' min={0} max={0.999999} step='any'
      value={played}
      onMouseDown={handleSeekMouseDown}
      onInput={handleSeekChange}
      onMouseUp={handleSeekMouseUp}
      />*/

    const handleDuration = time => {
	store.set('duration', time);
    }
    
    const handleProgress = state => {
	setProgressTime(state.played);
	setPlayed(parseFloat(state.played));
	store.set('played', state.played);
    }

    const handlePause = () => {
	if (isAdmin === true){
	    console.log('onPause');
	    setPlayerState(false);
	    setPlaying(false);
	}
    }

    const handlePlay = () => {
	if (isAdmin === true){
	    console.log('onPlay');
	    setPlayerState(true);
	    setPlaying(true);
	    if (timeOnLoad > 0 && onLoad === true){
		player.current.seekTo(parseFloat(timeOnLoad));
		setPlayed(parseFloat(timeOnLoad));
		store.set('played', timeOnLoad);
		setOnLoad(false);
	    }
	}
    }
    
    const handleEnded = () => {
	console.log('onEnded');
	if (playlist.length > 0){
	    deleteVideo(playlist[0]._id);
	    setUrl(playlist[0].url);
	    if (autoPlay === true)
		setPlaying(true);
	}
	else
	    setUrl('');
	player.current.seekTo(0);
    }

    const barStyle = {
	height: '8px',
	width: `${played * 100  }%`
    }
    
    return (
	    <div class={style.playerWrapper}>
	    {!isAdmin &&
	     <div class={style.playerOverlay} /> }
	    <ReactPlayer
	ref={player}
	controls={controls}
	url={url}
	playing={playing}
	onSeek={e => {
	    console.log('onSeek', e)
	    setSeekTime(e);
	}}
	onProgress={handleProgress}
	onPlay={handlePlay}
	onPause={handlePause}
        onEnded={handleEnded}
	onDuration={handleDuration}
	// Disable download button
	config={{ file: { attributes: { controlsList: 'nodownload' }  } }, {youtube: { playerVars: { showinfo: 0 }}} }
	// Disable right click
	onContextMenu={e => e.preventDefault()}
	class={style.player}
	/>
	    <div class={style.barGray} onClick={(e) => {
		const width = e.currentTarget.clientWidth;
		const offsetX = e.offsetX + e.target.offsetLeft - e.currentTarget.offsetLeft;
		console.log(offsetX / width);
		if (isAdmin === true)
		{
		    setSeekTime(parseFloat(offsetX / width));
		    player.current.seekTo(parseFloat(offsetX / width));
		    setPlayed(parseFloat(offsetX / width));
		}
	    }}>
	    <div class={style.barGreen} style={barStyle} />
	    </div>
	    {playing
	     ?<Pause onClick={handlePause} />
	     :<Play onClick={handlePlay} />
	    }
	{isAdmin === true &&
	 <Next onClick={handleEnded} />
	}
		    </div>
    );
}

export default VideoPlayer;

import { h } from 'preact';
import style from './style';
import { useState } from 'preact/hooks';

import Bubble from '../Bubble';

import Send from 'preact-icons/fa/paper-plane';
import Emoji from 'preact-icons/md/mood';

import 'emoji-mart/css/emoji-mart.css'
import { Picker } from 'emoji-mart'

import Markdown from 'preact-markdown';

import store from '../../store';

const ChatBox = ({messages, sendMessage, myID}) => {    
    const [textInput, setTextInput] = useState("");
    const [isEmojiShow, setIsEmojiShow] = useState(false);

    const [multiline, setMultiline] = useState(false);
    
    const fmtMSS = (s) => {
	return(s-(s%=60))/60+(9<s?':':':0')+s;
    }

    const trunc = (time) => {
	return time.substring(0, time.indexOf("."));
    }

    const stringToColor = (str) => {
	let hash = 0;
	for (let i = 0; i < str.length; i++) {
	    hash = str.charCodeAt(i) + ((hash << 5) - hash);
	}
	let color = '#';
	for (let n = 0; n < 3; n++) {
	    let value = (hash >> (n * 8)) & 0xFF;
	    color += (`00${  value.toString(16)}`).substr(-2);
	}
	return color;
    }

    const handleKeyDown = (e) => {
        if (e.key === 'Enter') {
            sendMessage(textInput, store.get('played', null, true));
	    setTextInput('');
        }
    }
    
    return (
	    <>
	    <div class={style.chatbox}>
	    {messages.length > 0 &&
	     messages.map((message) => {
		 let name = {
		     fontWeight: "600",
		     marginLeft: "6px",
		     color: stringToColor(message.username + message.userID.substring(message.userID.length-4, message.userID.length))
		 }
		 return (
		     <Bubble mine={myID === message.userID}>
		     {
			 myID === message.userID
			     ?<span style={name}>{message.username}</span>
			     :<span style={name}>{message.username}</span>
		     }
			 <p class={style.videoTime}>{trunc(fmtMSS(message.time * store.get('duration', null, true)))}</p>
			 <div class={style.message}>
			 <Markdown  markdown={message.message}  />
			 </div>
			 <span class={style.timestamp}>{new Date(message.date).toLocaleString()}</span>
			 </Bubble>
		 )
	     })
	    }
	</div>
	    {isEmojiShow &&
	     <Picker class={style.emojiPicker} onSelect={(e) => setTextInput(textInput + e.native)} />
	    }
	    <div class={style.wrapper}>
	    <Emoji onClick={() => setIsEmojiShow(!isEmojiShow)} size={24} />
	    {multiline
	     ?<textarea value={textInput} onChange={(e) => setTextInput(e.target.value.toString())} onKeyDown={handleKeyDown} />
	     :<textarea value={textInput} onChange={(e) => setTextInput(e.target.value.toString())} />
	    }
	    <Send onClick={() => { sendMessage(textInput, store.get('played', null, true)); setTextInput('') }} size={24} />
	    </div>
	    <div style="display: flex; margin-left: auto; font-weight: 600; font-size: small; align-items: center;">
	    <input style="height: 25px;" type="checkbox" value={multiline} onClick={() => setMultiline(!multiline)} />
	    <label>Touch Enter to send</label>
	    </div>
	    </>
    );
}

//<div class={style.message} style="background: white;resize: none; -webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; overflow: auto !important" dangerouslySetInnerHTML={{__html: message.message}}></div>
//<textarea name="msg" value={textInput} onChange={(e) => setTextInput(e.target.value.toString())} />
//	     :<input value={textInput} onInput={ (e) => setTextInput(e.target.value) } onKeyDown={handleKeyDown} />
export default ChatBox;
